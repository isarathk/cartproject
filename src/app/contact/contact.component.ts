import { Component, OnInit } from '@angular/core';
import { Contact } from '../contact';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactuser: Contact[] = [];

  constructor(private _route: Router) { }
  username:string;
  password:string;
  fname:string;
  mobile:number;
  message:string;
  ngOnInit() {
  }

  onSubmit(value: Contact) {
    console.log(value);
    alert("Your request has been sent!!!")
    this._route.navigate(["Home"]);
  }

}
