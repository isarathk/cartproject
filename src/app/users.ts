export class Users {
    public id:number;
    public fname:string;
    public mailid: string;
    public gender:string;
    public password:string;
    public city:string;
    public mobile:number;
    public pincode:number;
  }
