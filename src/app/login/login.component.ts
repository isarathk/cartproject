import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { Users } from '../users';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  errormsg: string;
  userlist: Users[] = [];

  constructor(public _route: Router,
    private loginService: LoginService) { }

  ngOnInit() {

    //https://alligator.io/js/introduction-localstorage-sessionstorage/
  }

  // Checking User exists in database or not
  isUsersExists(): void {
    let lstuser = this.userlist.filter(a => a.mailid == this.username);
     
    if (lstuser && lstuser.length > 0) {
      let user = lstuser[0];
      if (user.fname == "admin" && user.password == this.password) {
        this._route.navigate(["Home/UsersList"]);
        localStorage.setItem('username', user.fname);
      }
      else if (user.password == this.password) {
        this._route.navigate(["Home"]);
        localStorage.setItem('username', user.fname);
      }
      else {
        alert("Invalid Password!!!");
      }
    }
    else {
      alert("Invalid User or Email!!!");
    }
  }
  onSubmit(value: any) {

    this.loginService.getUsers()
      .subscribe(users => {
        this.userlist = users;
        this.isUsersExists();
      }

      );
  }
}
