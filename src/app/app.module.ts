import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, Route } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ForgotPwdComponent } from './forgot-pwd/forgot-pwd.component';
import { RegistrationComponent } from './registration/registration.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { OrdersComponent } from './order/orders.component';
import { UserListComponent } from './user-list/user-list.component';
import { CartsComponent } from './carts/carts.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductsComponent } from './products/products.component';
import { LoginService } from './login/login.service';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { SharedService } from './shared.service';

const approute: Routes = [
  { path: '', redirectTo: 'Login', pathMatch: 'full' },
  { path: 'Login', component: LoginComponent },
  { path: 'Registration', component: RegistrationComponent },
  { path: 'ForgotPwd', component: ForgotPwdComponent },
  {
    path: 'Home', component: HomeComponent,
    children: [
      { path: '', component: ProductsComponent },
      { path: 'Contact', component: ContactComponent },
      { path: 'Orders', component: OrdersComponent },
      { path: 'UsersList', component: UserListComponent },
      { path: 'Carts', component: CartsComponent },
      { path: 'CheckOut', component: CheckoutComponent },
      { path: 'Products', component: ProductsComponent },
      { path: 'ItemDetails/:id', component: ItemDetailsComponent }
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPwdComponent,
    RegistrationComponent,
    HomeComponent,
    ContactComponent,
    OrdersComponent,
    UserListComponent,
    CartsComponent,
    CheckoutComponent,
    ProductsComponent,
    ItemDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(approute),
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService)
  ],
  providers: [LoginService, InMemoryDataService, SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
