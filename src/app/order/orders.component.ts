import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
import { Product } from '../product';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  orderItems: Product[];

  constructor(private sharedService: SharedService) { }

  ngOnInit() {
    this.sharedService.getOrders()
    .subscribe(orders => {
      this.orderItems = orders;      
    }

    );
  }

}
