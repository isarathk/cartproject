import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductService } from './product.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductService]
})
export class ProductsComponent implements OnInit {

  constructor(private productService: ProductService,
    private _route: Router,
    private activatedRoute: ActivatedRoute) { }

  filteredItems: Product[];//an array of ‘Product’ used to stock the result of the search process executed by ‘FilterByName’ function.
  pages: number;//4  is a maximum of Page numbers that can be displayed in pagination bar (it’s the size of ‘pagesIndex’ array).
  pageSize: number = 5;//5  indicate the number of entries per page.
  pageNumber: number = 0;//is the maximum number of page can we deduce from the ‘ProductList’ in terms of ‘pageSize’ value,
  currentIndex: number = 1;//is the index of the current selected page.
  items: Product[];//the content of selected page
  pagesIndex: Array<number>;//have the series of numbers shown in the pagination bar.
  pageStart: number = 1;//this the start index page in the pagination bar.
  inputName: string = '';//is used to search a new list of Product which the value of the attribute name for each one contains the text in ‘inputName’.
  user: string;//represent the login username.

  ngOnInit() {

    this.productService.getProducts()
      .subscribe(products => {
        this.items = products;
        this.filteredItems = products;
      }

      );
  }

  FilterByName() {
    this.filteredItems = [];
    if (this.inputName != "") {
      this.items.forEach(element => {
        if (element.Name.toUpperCase().indexOf(this.inputName.toUpperCase()) >= 0) {
          this.filteredItems.push(element);
        }
        else {
          this.filteredItems.push();
        }
      });
    } else {
      this.filteredItems = this.items;// text box empty then call to list array.
    }

    console.log(this.filteredItems);

  }

  onItemClick(item: Product) {
    this._route.navigate(['./ItemDetails/' + item.id], { relativeTo: this.activatedRoute });
  }

}
