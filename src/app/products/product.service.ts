import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { Users } from '../users';
import { Product } from '../product';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class ProductService {

  private productsUrl = 'api/products';

  constructor(private http: HttpClient) { }

  
  getProducts() {
    return this.http.get<Product[]>(this.productsUrl)
  }

}
