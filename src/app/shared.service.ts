import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { Product } from './product';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class SharedService {

  private productUrl = 'api/products';
  private cartUrl = 'api/cartItems';
  private ordersUrl = 'api/orders';
  private dataurl = 'api/datas';


  constructor(private http: HttpClient) { }

  getProducts() {
    return this.http.get<Product[]>(this.productUrl)
  }

  addToCart(item: Product): Observable<Product> {  
    return this.http.post<Product>(this.cartUrl, item, httpOptions);
  }

  getOrders(){
    return this.http.get<Product[]>(this.ordersUrl)
  }
  createOrders(order: Product): Observable<Product> {
    return this.http.post<Product>(this.ordersUrl, order, httpOptions);
  }

  getCarts() {
    return this.http.get<Product[]>(this.cartUrl)
  }

}
