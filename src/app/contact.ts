export class Contact {
    public fname:string;
    public username:string;
    public mobile:number;
    public message:string;
}
