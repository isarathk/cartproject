import { Component, OnInit } from '@angular/core';
import { Users } from '../users';
import { Router } from '@angular/router';
import { RegistrationService } from './registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [RegistrationService]
})
export class RegistrationComponent implements OnInit {
  userlist: Users;

  constructor(private _route:Router,
              private regService: RegistrationService) { }

  ngOnInit() {
    this.userlist = new Users();
  }

  private navigateToLogin(value: Users){
    alert("User " + value.fname + " Created Successfully!!!")
    this._route.navigate(["Login"]);
  }

  onSubmit(value: Users) {     
    console.log(value);
    this.regService.createeUser(value).subscribe((v) => this.navigateToLogin(value));
        
  }

}
