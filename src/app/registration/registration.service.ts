import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { Users } from '../users';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable()
export class RegistrationService {

  private heroesUrl = 'api/users';

  constructor(private http: HttpClient) { }

  createeUser(user: Users): Observable<Users> {
    return this.http.post<Users>(this.heroesUrl, user, httpOptions);
  }

}
