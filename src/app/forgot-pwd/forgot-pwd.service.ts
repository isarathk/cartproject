import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { Users } from '../users';

@Injectable()
export class ForgotPwdService {

  private heroesUrl = 'api/users';

  constructor(private http: HttpClient) { }
  
  
  // searchUser(mailID: string): Observable<Users[]> {
  //   if (!mailID.trim()) {
  //     // if not search term, return empty hero array.
  //     return of([]);
  //   }
  //   return this.http.get<Users[]>(`api/users/?mailid=${mailID}`);
  // }

  searchUser(mailID: string): Observable<Users[]> {
    
    return this.http.get<Users[]>(this.heroesUrl);
  }
}
