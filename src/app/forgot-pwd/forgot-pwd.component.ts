import { Component, OnInit } from '@angular/core';
import { ForgotPwdService } from './forgot-pwd.service';
import { Users } from '../users';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-pwd',
  templateUrl: './forgot-pwd.component.html',
  styleUrls: ['./forgot-pwd.component.css'],
  providers: [ForgotPwdService]
})
export class ForgotPwdComponent implements OnInit {

  constructor(private fgtService: ForgotPwdService, private _route: Router) { }
  mailID: string;
  userlist: Users[];
  ngOnInit() {

  }

  // Checking User exists in database or not
  isUsersExists(): void {

    if (this.userlist != null && this.userlist.length > 0) {

      let lstuser = this.userlist.filter(a => a.mailid == this.mailID);

      if (lstuser != null && lstuser.length > 0) {
        this._route.navigate(["Login"]);
      }
      else {
        alert("User Email doen't exist!!!");
      }

    }
    else {
      alert("User Email doen't exist!!!");
    }
  }
  onSubmit(value: any) {

    this.fgtService.searchUser(this.mailID)
      .subscribe(users => {
        this.userlist = users;
        console.log(this.userlist);
        this.isUsersExists();
      }

      );
  }

}
