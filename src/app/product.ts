export class Product{
    public id:number;
    public orderId:number;
    public Name:string;
    public url:string;
    public longDesc:string;
    public price:number;      
    public orderedQty:number;   
  }