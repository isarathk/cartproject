import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
import { Product } from '../product';

@Component({
  selector: 'app-carts',
  templateUrl: './carts.component.html',
  styleUrls: ['./carts.component.css']
})
export class CartsComponent implements OnInit {

  cartsItemsList: Product[] = [];

  constructor(private sharedService: SharedService) { }

  ngOnInit() {

    this.sharedService.getCarts()
      .subscribe(items => {
        this.cartsItemsList = items;
      }

      );

  }

}
