import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {

  items: Product;
  user: string = '';
  list: Product[];
  CartNo1: any;
  CartNo: number = 0;
  cart2: Product[] = [];
  saved: string;
  selectedQty: number = 1;
  result: number;
  selectedItem: any;

  constructor(private _route: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService) {

      this.items = new Product();
     }

  getSelectedItem(id: number): void {

    this.items = this.list.filter(a => a.id == id)[0];
  }

  ngOnInit() {

    var id = this.route.snapshot.params["id"];

    this.sharedService.getProducts()
      .subscribe(items => {
        this.list = items;
        this.getSelectedItem(id);
      }

      );

  }

  navigateToCartsPage() {    
    this._route.navigate(["../../Carts"], { relativeTo: this.route });
  }

  navigateToCheckoutPage() {
    
    this._route.navigate(["../../CheckOut"], { relativeTo: this.route });
  }

  addtocart() {
    this.items.orderedQty = this.selectedQty;
    this.sharedService.addToCart(this.items).subscribe((v) => 
    {    
      this.navigateToCartsPage()
    });
  }

  BuyNow(items: Product) {

    // Get Existing Orders



    // let orderItem = new Orders();
    // orderItem.id = this.items.id;
    // orderItem.itemDesc = this.items.Name;
    // orderItem.itemID = this.items.id;
    // orderItem.orderedQty = this.selectedQty;
    // orderItem.image = this.items.url;
    // orderItem.price = this.items.price;

    this.sharedService.createOrders(this.items).subscribe((v) => 
    {    
      this.navigateToCheckoutPage()
    });
  }

}
