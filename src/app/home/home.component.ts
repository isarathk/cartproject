import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  showUsers: boolean = false;
  user: string = '';
  selectedQty: number = 0;
  constructor(private _route: Router) { }

  ngOnInit() {

    this.user = localStorage.getItem('username');

    if (this.user == "admin")
      this.showUsers = true;
    else
      this.showUsers = false;
  }

  logout() {
    localStorage.removeItem("username");
    this._route.navigate(["Login"]);
  }

}
