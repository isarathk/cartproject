import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {

    createDb() {
        const cartItems = [];
        const orders = [];
        const users = [
            { id: 11, fname: 'Sarath', mailid: 'sarath@gmail.com', gender: 'Male', password: '1234', city: 'Hyd', mobile: 9874563210, pincode: 12345 }
        ];      

        const products = [
            {
                id: 1,
                orderId: 0,
                Name: "Bagger Daves",
                url: "./assets/Images/1.jpg",
                longDesc: "Bagger Dave's Burger Tavern Inc. ",
                price: 200, 
                orderedQty: 0
            },

            {
                id: 2,
                orderId: 0,
                Name: "Gulab jamun",
                url: "./assets/Images/2.jpg",
                longDesc: "Gulab jamun (also spelled gulaab jamun) are a milk-solid-based South Asian sweet, particularly popular in the Indian subcontinent, notably India, Nepal Pakistan, Sri Lanka and Bangladesh, as well as Myanmar. It is also common in Mauritius, Fiji, southern and eastern Africa, Malay Peninsula, and the Caribbean countries",
                price: 4,                
                orderedQty: 0               
            },
            {
                id: 3,
                orderId: 0,
                Name: "Cheerio",
                url: "./assets/Images/3.jpg",
                longDesc: "Cheerio or Cheerios may refer to: Cheerio, a British English or Hiberno-English informal parting expression; Cheerios, a breakfast cereal with a number of variations. Honey Nut Cheerios, the second variation created. Cheerio (company), the company that makes the drink below. Cheerio (drink), a Japanese soft drink.",
                price: 200, 
                orderedQty: 0
            },
            {
                id: 4,
                orderId: 0,
                Name: "Sun-Maid Growers",
                url: "./assets/Images/4.jpg",
                longDesc: "Sun-Maid Growers of California is a privately owned American cooperative of raisin growers headquartered in Kingsburg, California. Sun-Maid is the largest raisin and dried fruit processor in the world. As a cooperative, Sun-Maid is made up of approximately 850 family farmers who grow raisin grapes within 100 miles",
                price: 132,              
                orderedQty: 0,
            }, 
            {
                id: 5,
                orderId: 0,
                Name: "Horlicks",
                url: "./assets/Images/5.jpg",
                longDesc: "Horlicks is usually taken to be a substitute for the profanity bollocks. This use was exploited by the company in a 1990s advertising campaign, in which a harassed housewife exclaims Horlicks in a context where a stronger term could have been expected, thus widening the term's exposure and usage for a while.",
                price: 1000,               
                orderedQty: 0              
            },
            {
                id: 6,
                orderId: 0,
                Name: "Maggi",
                url: "./assets/Images/6.jpg",
                longDesc: "Maggi is an international brand of seasonings, instant soups, and noodles that originated in Switzerland in late 19th century. The Maggi company was acquired by Nestlé in 1947. Contents. [hide]. 1 Company history; 2 Products. 2.1 Cube; 2.2 Seasoning sauce; 2.3 Noodles. 2.3.1 Maggi Cuppa Mania. 2.4 Dehydrated soup.",
                price: 11022,               
                orderedQty: 0               
            },
            {
                id: 7,
                orderId: 0,
                Name: "Chocolate brownie",
                url: "./assets/Images/7.jpg",
                longDesc: "A chocolate brownie (commonly referred to as simply brownie) is a square, baked, chocolate dessert. Brownies come in a variety of forms and may be either fudgy or cakey, depending on their density. They may include nuts, frosting, cream cheese, chocolate chips, or other ingredients. A variation made with brown sugar rather than chocolate in the batter is called a blonde brownie or blondie.",
                price: 10000,              
                orderedQty: 0
            },
            {
                id: 8,
                orderId: 0,
                Name: "Unicorn",
                url: "./assets/Images/8.jpg",
                longDesc: "Unicorn The gentle and pensive maiden has the power to tame the unicorn, fresco, probably by Domenico Zampieri, c. 1602 (Palazzo Farnese, Rome)Grouping	MythologySimilar creatures Re'em, Indrik, Shadhavar, Camahueto, Karkadann Mythology	WorldwideOther name(s)	Monocerus",
                price: 165645,             
                orderedQty: 0                
            },
            {
                id: 9,
                orderId: 0,
                Name: "Vegetable",
                url: "./assets/Images/9.png",
                longDesc: "everyday usage, vegetables are certain parts of plants that are consumed by humans as food as part of a savory meal. Originally, the traditional term (still commonly used in biology) included the flowers, fruit, stems, leaves, roots, tubers, bark, seeds, and all other plant matter, although modern-day culinary usage of the term vegetable may exclude food derived from plants such as fruits, nuts, and cereal grains, but include seeds such as pulses; the term vegetable is somewhat arbitrary, and can be largely defined through culinary and cultural tradition.",
                price: 165648,
                orderedQty: 0
            },
            {
                id: 10,
                orderId: 0,
                Name: "Eggs",
                url: "./assets/Images/10.jpg",
                longDesc: "Eggs seasoned with ground cloves on a ham sandwich, with a side of portobello mushroomsEggs are laid by female animals of many different species, including birds, reptiles, amphibians, mammals, and fish, and have been eaten by humans for thousands of years.[1] Bird and reptile eggs consist of a protective eggshell, albumen (egg white), and vitellus (egg yolk), contained within various thin membranes. The most commonly consumed eggs are chicken eggs. Other poultry eggs including those of duck and quail are also eaten. Fish eggs are called roe and caviar.",
                price: 165645,
                orderedQty: 0
            }];

        return { users, products, orders, cartItems };
    }
}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/